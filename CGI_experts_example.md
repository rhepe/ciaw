# The CGI Experts. 
## Infrastructure and Management

A Company that creates visual Special Effects for international TV and Movie productions operates a cluster of 200 Servers at it’s office in Stuttgart. This Cluster is used to render 2D and 3D-CGI-Effects (CPU-Rendering, *not* GPU Rendering). 

Unfortunately, this company has to deal with at least a dozen different Software Packages for CGI-Calculations that also require different Operating Systems. At least three different Windows-Versions (Server 2003R2, 2008R2 and 2012) are used as well as two different Linux Distros (RHEL and Debian).

Currently, the CGI-Clusters Admin Team uses a bunch of different Deployment Tools to PXE-Install the OS and Software packages on the Cluster. This process takes to much time, is inflexible and needs too much personel.

Task: Create a flexible Infrastructure, that allows flexible Cluster Configs and centrally manages the Cluster Deployment and Configuration.

Facts:
 
 - A shared Storage Platform for this Cluster is preconfigured (10 GBit/s iSCSI) and should not be changed. 
 
 - A partners quote for a solution using Vsphere has been already rejected by the customer due to costs

