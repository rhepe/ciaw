# Team 1
## Dropbox source in

An international constructing company uses a public cloud storage system (Dropbox) for all employees and partners to store and share information, regarding new regulations the company decided to migrate this solution to an on-premise solution without degrading the customer experience.
The actual size of the environment is 5 k internal and approx 10 k external user (partner and customers) with a total storage of approx. 400 TB with an expected grow of 20 % YoY.
The solution should be high available and disaster tolerant, optional there should be an opportunity to scale the front end to an IaaS Cloud Service Provider.
Hint: RED HAT only offers the infrastructure for this you have to look for an 3rd party  application to fulfill the need (e.g. OwnCloud/Nextcloud/Seafile ..)

### Tasks:
- Architect a storage solution to be run inside the companies datacenter
- Architect a dropbox-alike access-layer to that storage for inhouse and external users


# Team 2
## CGI Experts Storage Upgrade
Besides the CGI-Cluster (see Day 2 scenario), the Special Effects Company runs 50 Workstations for their creative Engineers. Again, depending on the usecase, there are a lot of different Software packages and OSes in Place. Mac-OS-X, Windows 7/10 and Ubuntu are used on the different Workstation. Each Production the company is Working on requires up to 50 TB of Data, typically stored in Sequence Chunks from 500 to 5000 GB. The Company usually works on 5 to 10 Productions simultaniously All this data has to be stored on a fast shared Storage, that can be accessed from all Workstation, regardless of the OS as well as from all Cluster Nodes to be rendered.

### Tasks:

- Architect a Storage solution that fits to the described purpose. Keep in Mind, that the company plans to double the number of Engineers and Workstations as well as the number of concurrent projects within the next 3 Years.
- Describe the Update Path, how the storage Solution could scale over time.
- Architect an extension to that storage, that will enable the CGI-Cluster to access this shared storage, so workstations and servers can share Sequence Chunks instead of moving the Date between the Client and Server Storage
