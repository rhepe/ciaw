# Red Hat Cloud Infrastructure Architecture Workshop (CIAW)
Version 1.7, 27. July 2017, astolzen
## Description:
This workshop is based on the feedback from attendees of ILT-Trainings we did last year, a lot of partners requested a workshop to answer questions about designing Red Hat's cloud solutions and integrate them in existing real-world IT environments. Another request was that partners need more arguments to sell Red Hat's cloud solutions into the large and complex environment. The idea behind this 3-day workshop is that - based on the presentation of Red Hat's products - the attendees develop a complete technical sales speech and are able to estimate the effort to do a POC for such an environment. This should give our partners a good technical foundation to sell Red Hat's Cloud Solutions  
## Target Audience:
Partner with some technical knows how they have already or expect the request to design a solution for a hyper converged datacenter. The attendees should have some experience in designing Virtual datacenter with traditional infrastructures like VMware, MS HyperV or RHEV and some know-how in one or more management products. The workshop addresses primary system architects and strategists and explicit Red Hat know how is appreciated but not explicit required.
## Prerequisites
There are no explicit prerequisites for this course, the only one is that the attends is willing to share his experience and work together in a team with others on solutions.  
## Outcome:
Based on 3 predefined real world scenarios the attendees develop complete solutions and a presentation that covers Virtualization, container, management and storage. If anyone wants to discuss an actual scenario we could try to this also.
with this knowledge, the attendees are able to create a system architecture and a sales tactic to win upcoming cloud opportunities.
## Detailed Agenda:
### Day One: Infrastructure/Virtualization Day
| Time | Topic |
|---|---|
| 09:30 - 10:00 | Welcome |
| 10:00 - 10:30	| Introduction to the Course, Agenda, WorkShop Overview |
| 10:30 - 12:30	| Overview Red Hat Virtualization Technologies RHEV, OpenStack, OpenShift, Use-cases, Architecture, Competition, |
| 12:30 - 13:30	| Lunch Break |
| 13:30 - 14:00	| Present the Scenarios for the Workshop, Group Definition |
| 14:00 - 15:30	| GroupWork 1/3 „Create a proposal for a new Virtualization	strategy based on the information you already have“ |
| 15:30 - 15:45	| BREAK |
| 15:45 - 16:30	| Presentations Each group present their solution |
| 16:30 - 17:30	| Discussion of the result  and conclusion |
#### HANDOUT
 - WhitePapers: _RHEV, OpenStack, OpenShift_
 - Sizing Documents: _RHEV, OpenStack_
 - Architecture Example: _RHEV, OpenStack_
### Day Two: Management Solutions
| Time | Topic |
|---|---|
|09:00 - 09:30|Recap Day ONE|
|09:30 - 11:00|Presentation Red Hat Management Solutions RH Satellite, Ansible, CloudForms Use Cases, how they fit together|
|11:00 - 11:15|BREAK|
|11:15 - 12:00|LIVE PRESENTATION MANAGEMENT	Based on CloudForms Open|
|12:00 - 12:30|Introduction WorkShop Day 2 „How to add a management stack to our scenario“|
|12:30 - 13:30|BREAK|
|13:30 - 15:00|GroupWork 2/3 „How to add a management stack to our 						scenario“|
|15:00 - 15:15|BREAK|
|15:15 - 16:15|Presentations of each group|
|16:15 - 17:00|Discussion and conclusion Day 2|

#### HANDOUT:
 - WhitePapers: _Satellite, CloudForms, Ansible_
 - Sizing: _CloudForms_
 - Architecture Example: _Cloudforms_
### Day Three: Storage Day
| Time | Topic |
|---|---|
|09:00 - 09:30|Recap Day 2|
|09:30 - 11:00|Presentation: Software defined Storage / CEPH and Cluster|
|11:00 - 11:15|Break|
|11:15 - 12:30|GroupWork 3/3 „Add an Software defined storage solution to your scenario“|
|12:30 - 13:30|Break|
|13:30 - 14:30|Final Presentation of the scenarios from each team|
|14:30 - 16:00|Discussion and lessons learned|
|16:00 - 16:30|Q & A, feedback|

#### HANDOUT:
 - WhitePapers: _CEPH, Gluster_
 - Sizing: _CEPH_
 - Architecture Example: _CEPH Cluster_
